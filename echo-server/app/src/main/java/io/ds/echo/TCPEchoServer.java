package io.ds.echo;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPEchoServer {
    public static void main (String args[]) {
       int serverPort = 7896; 
       try(ServerSocket listenSocket = new ServerSocket(serverPort)) {	
          while(true) {
            System.out.println("Waiting for connection requests ...");
             Socket clientSocket = listenSocket.accept();
             System.out.println("Connected");
             DataInputStream in = new DataInputStream( clientSocket.getInputStream());
             DataOutputStream out = new DataOutputStream( clientSocket.getOutputStream());
             System.out.println("Read data");
             String data = in.readUTF();
             System.out.println("Write data");                
             out.writeUTF(data);
          }
       } catch(Exception e) {
          System.out.println("Listen :"+e.getMessage());
       }
    }
 }
 
